$(window).scroll(function() {
	var yScroll = $(this).scrollTop();
	var opacity, z;
	$(".title").css({
		"transform": "translateY(" + yScroll / 2 + "%)"
});

	opacity = Math.min(Math.max(1 - (yScroll - 100) / 150, 0), 1);
	z = yScroll > 250 ? -10 : 200;
	$(".links").css({ "opacity": opacity, "z-index": z}); 
});